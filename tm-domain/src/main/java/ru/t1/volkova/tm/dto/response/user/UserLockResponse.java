package ru.t1.volkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResultResponse;
import ru.t1.volkova.tm.dto.response.AbstractUserResponse;
import ru.t1.volkova.tm.model.User;

@NoArgsConstructor
public final class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

}
