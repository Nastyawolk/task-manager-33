package ru.t1.volkova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResponse;
import ru.t1.volkova.tm.model.Project;

@Getter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

}
