package ru.t1.volkova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class UserListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public UserListRequest(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}
