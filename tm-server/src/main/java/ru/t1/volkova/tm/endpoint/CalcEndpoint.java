package ru.t1.volkova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.endpoint.ICalcEndpoint;
import ru.t1.volkova.tm.api.service.IServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.ICalcEndpoint")
public class CalcEndpoint extends AbstractEndPoint implements ICalcEndpoint {

    public CalcEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

}
