package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.user.*;
import ru.t1.volkova.tm.dto.response.user.UserLoginResponse;
import ru.t1.volkova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.volkova.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpointClient {

    @NotNull UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull UserProfileResponse profile(@NotNull UserProfileRequest request);

}
