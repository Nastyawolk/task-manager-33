package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.volkova.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove task by index.";

    @NotNull
    private static final String NAME = "task-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
